import { v4 as uuid } from 'uuid';

// Definitions

// With Namespace -> Persistent
export type IPersistentTableId = {
  id: string;
  namespace: string;
};

// Without Namespace -> Temporary
export type ITemporaryTableId = string;

// Could either be Persistent or Temporary -> Deciding
export type ITableId = IPersistentTableId | ITemporaryTableId;

// Helpers

// Checking whether a table
export const isTemporaryTable = (
  table: ITableId,
): table is ITemporaryTableId => typeof table === 'string';

export const PersistentTableId = (namespace: string, id?: string): ITableId => ({
  id: id || uuid().toString(),
  namespace,
});

export const TemporaryTableId = (): ITableId => uuid().toString();

