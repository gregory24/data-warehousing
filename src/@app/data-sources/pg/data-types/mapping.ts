import { ColumnType } from '../../../data-shapes';

export const POSTGRES_TO_APPLICATION_TYPES: { [type: string]: ColumnType } = {
  bool: ColumnType.Boolean,
  int8: ColumnType.Integer,
  integer: ColumnType.Integer,
  serial8: ColumnType.Integer,
  bit: ColumnType.Integer,
  int: ColumnType.Integer,
  int4: ColumnType.Integer,
  bigint: ColumnType.Integer, // Could be BigInt (More reliable)
  numeric: ColumnType.Integer,
  pg_lsn: ColumnType.Integer,
  pg_snapshot: ColumnType.Integer,
  int2: ColumnType.Integer,
  serial2: ColumnType.Integer,
  serial4: ColumnType.Integer,
  float: ColumnType.Float,
  float4: ColumnType.Float,
  float8: ColumnType.Float,
  money: ColumnType.Float,
  decimal: ColumnType.Float,
  date: ColumnType.Timestamp,
  timestamp: ColumnType.Timestamp,
  timestamptz: ColumnType.Timestamp,
  json: ColumnType.Json,
  jsonb: ColumnType.Json,
  uuid: ColumnType.SystemId,
};

export const APPLICATION_TO_POSTGRES_TYPES: { [type: string]: string | null } =
  {
    [ColumnType.Boolean]: 'bool',
    [ColumnType.Integer]: 'integer',
    [ColumnType.Float]: 'float8',
    [ColumnType.String]: 'text',
    [ColumnType.Timestamp]: 'timestamptz',
    [ColumnType.Json]: 'jsonb', // SUPER-IMPORTANT: Using JSONB to support comparisons
    [ColumnType.SystemId]: 'uuid',
    // Note: Separate BigInt could be added to support large values if necessary
  };

export const postgresToApplicationType = (type: string): ColumnType =>
  POSTGRES_TO_APPLICATION_TYPES[type] || ColumnType.String;

export const applicationToPostgresType = (fromType: ColumnType): string => {
  const toType = APPLICATION_TO_POSTGRES_TYPES[fromType];

  if (toType === null)
    throw new Error(`Impossible to convert data-type to Postgres: ${fromType}`);

  return toType;
};
