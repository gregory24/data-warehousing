import pgFormat from 'pg-format';
import { ITableId } from '../../../data-tables';
import { IColumn, IColumnName } from '../../../data-shapes';

// Note: Removing quotes to prevent duplicate nested quotes from being used
const sanitizePostgresName = (name: string): string =>
  pgFormat.ident(name.replace(/"/g, ''));

export const columnNameInPostgres = (column: IColumn | IColumnName): IColumnName =>
  typeof column === 'string' ? sanitizePostgresName(column) : sanitizePostgresName(column.name);

export const columnNamesInPostgres = (
  columns: (IColumn | IColumnName)[],
): IColumnName[] => columns.map(columnNameInPostgres);

export const tableNameInPostgres = (tableName: ITableId): string => {
  if (typeof tableName === 'string') {
    return sanitizePostgresName(tableName);
  }

  const ns = sanitizePostgresName(tableName.namespace);
  const table = sanitizePostgresName(tableName.id);

  return `${ns}.${table}`;
};
