import { Client, ClientConfig } from 'pg';

export interface IPostgresClientOptions {
  hashJoin: boolean;
  mergeJoin: boolean;
}

export const DEFAULT_POSTGRES_CLIENT_OPTIONS: IPostgresClientOptions = {
  hashJoin: true,
  mergeJoin: true,
};

export const initializePostgresClient = async (
  config: ClientConfig,
  options: Partial<IPostgresClientOptions> = {},
): Promise<Client> => {
  const fullOptions: IPostgresClientOptions = {
    ...DEFAULT_POSTGRES_CLIENT_OPTIONS,
    ...options,
  };

  const client = new Client(config);

  await client.connect();

  // Disabling Hash-Join for better performance
  /* istanbul ignore next */
  if (!fullOptions.hashJoin) {
    await client.query('SET enable_hashjoin = FALSE;');
  }

  // Disabling Merge-Join for better performance
  /* istanbul ignore next */
  if (!fullOptions.mergeJoin) {
    await client.query('SET enable_mergejoin = FALSE;');
  }

  return client;
};

export const usePostgresClient = async (
  config: ClientConfig,
  options: Partial<IPostgresClientOptions> = {},
): Promise<[Client, () => Promise<void>]> => {
  const client = await initializePostgresClient(config, options);

  const disconnect = async () => {
    await client.end();
  };

  return [client, disconnect];
};
