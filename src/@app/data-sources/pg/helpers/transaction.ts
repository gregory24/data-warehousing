import { Client } from 'pg';

export const withTransactionInPostgres = async <T>(
  client: Client,
  processor: (client: Client) => Promise<T>,
  callback?: () => Promise<any> | any,
): Promise<T> => {
  try {
    await client.query('BEGIN;');

    const result = await processor(client as any); // Same querying mechanism

    await client.query('COMMIT;');

    return result;
  } catch (error: any) {
    await client.query('ROLLBACK;');

    throw error;
  } finally {
    if (callback) {
      await callback();
    }
  }
};
