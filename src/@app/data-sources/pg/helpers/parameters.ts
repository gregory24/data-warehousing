export const parameterInPostgresFor = (
  parameterNumber: number,
  parameterDataType: string,
): string => `CAST($${parameterNumber} AS ${parameterDataType})`;

export const valueParametersInPostgresFrom = (
  columnDataTypes: string[],
  count: number,
  offset = 0,
): string => {
  const insertionParameters: string[] = [];
  let recordParameters: string[];

  // Note: It may be better to use offsets without any logic - Just as numerical values
  let parameterNumber: number = columnDataTypes.length * offset + 1; // For using parameter-values in multiple places

  for (let i = 0; i < count; i++) {
    recordParameters = [];

    for (const columnType of columnDataTypes) {
      recordParameters.push(
        parameterInPostgresFor(parameterNumber++, columnType),
      );
    }

    insertionParameters.push(`(${recordParameters.join(', ')})`);
  }

  return insertionParameters.join(', ');
};

export const listParametersInPostgresFrom = (
  dataType: string,
  count: number,
  offset = 0,
): string => {
  const recordParameters: string[] = [];

  let parameterNumber: number = offset + 1;

  for (let i = 0; i < count; i++) {
    recordParameters.push(parameterInPostgresFor(parameterNumber++, dataType));
  }

  return `(${recordParameters.join(', ')})`;
};
