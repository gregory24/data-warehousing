import csvStringify from 'csv-stringify';
import { COPY_DELIMITER, COPY_ESCAPE, COPY_QUOTE } from './copy-constants';
import { POSTGRES_FALSE_LITERAL, POSTGRES_TRUE_LITERAL } from '../data-types';

// REFACTORING: COMMON LOGIC FOR TRANSFORMING INTO CSV

export const CopyCsvTransform = (columns: string[]) =>
  csvStringify({
    // Format
    delimiter: COPY_DELIMITER,

    // Sanitization
    // Specifying quote for strings (Grouping all characters together to prevent delimiter / escape from interfering)
    quote: COPY_QUOTE,
    quoted_string: true, // Quoting ALL strings for consistency and reliability
    // quoted_empty: true, // DO NOT USE - NULLs should just be COMPLETELY empty
    escape: COPY_ESCAPE, // Specifying escape-character -> Allows using quotes INSIDE THE input (Gets escaped properly)

    // Positioning and Ordering
    header: true, // Ordering all fields under the same header for deciding their positions
    columns, // Specifying actual fields / columns of the input-data and their Order
    //  - Allows using raw-data / records without knowing position of each Field / Column
    //  -> Requires specifying the EXACT SAME columns in the EXACT SAME order for "COPY [table]([columns]) FROM"

    // Casting / Transforming
    cast: {
      // Making sure bool is properly casted and is NOT converted into NULL
      boolean(value: boolean) {
        return value ? POSTGRES_TRUE_LITERAL : POSTGRES_FALSE_LITERAL;
      },
      date(value: Date) {
        return value.toISOString(); // Using ISO format
      },
    },
  });
