import { pipeline, Readable, Writable } from 'stream';
import { promisify } from 'util';
import { Client, PoolClient } from 'pg';
import { from as copyFrom } from 'pg-copy-streams';

import { COPY_DELIMITER, COPY_ESCAPE, COPY_QUOTE } from './copy-constants';
import { CopyCsvTransform } from './copy-csv-transform';
import { IDataShape } from '../../../data-shapes';
import { ITableId } from '../../../data-tables';
import { copyQueryInPostgresFrom, ICopyWritePostgresSettings } from './copy-query';

const pipelineAsync = promisify(pipeline);

// Important: Working on Pool-Clients instead of Pools in order to properly use Transactions (Same Session / Connection)
export const copyWriteToTableInPostgres = async (
  client: Client | PoolClient,
  tableId: ITableId,
  shape: IDataShape,
  sourceStream: Readable,
): Promise<void> => {
  const settings: ICopyWritePostgresSettings = {
    delimiter: COPY_DELIMITER,
    escape: COPY_ESCAPE,
    quote: COPY_QUOTE,
    header: true,
  };
  const columns = shape.columns.map((c) => c.name);

  const csvTransform = CopyCsvTransform(columns);

  const copyQuery = copyFrom(
    copyQueryInPostgresFrom(tableId, columns, settings),
  );
  const destinationStream = client.query(copyQuery) as Writable;

  await pipelineAsync(sourceStream, csvTransform, destinationStream);
};
