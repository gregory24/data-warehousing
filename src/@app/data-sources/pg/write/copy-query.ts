import { ITableId } from '../../../data-tables';
import { columnNameInPostgres, tableNameInPostgres } from '../helpers';
import { trimQuery } from '../../common';

export interface ICopyWritePostgresSettings {
  delimiter: string;
  quote: string;
  escape: string;
  header: boolean;

  // SHOULD NOT BE SUPPLIED / MODIFIED (Prevents empty-strings from being used)
  // null: string;
}

export const copySqlSettingsFrom = (
  settings: ICopyWritePostgresSettings,
): Record<string, string> => ({
  // Accepting Text-Input and using CSV-Specific settings (Delimiters, Quotes, Escape, etc.)
  FORMAT: 'CSV',

  // Formatting
  DELIMITER: `'${settings.delimiter}'`,
  QUOTE: `'${settings.quote}'`,
  ESCAPE: `'${settings.escape}'`,

  // Specifying that input has a header -> Simply ignores the first line (Does NOT decide ordering)
  //   -> Must be specified in the table-definition
  HEADER: settings.header ? 'TRUE' : 'FALSE',

  // SHOULD NOT BE PROVIDED
  // 'NULL': settings.null, // IMPORTANT: DO NOT USE - PREVENTS FROM RECOGNIZING NULLs where there are Empty-Strings
});

export const copyQueryInPostgresFrom = (
  tableId: ITableId,
  columns: string[],
  settings: ICopyWritePostgresSettings,
): string => {
  const sqlColumns = columns.map(columnNameInPostgres).join(', ');
  const copySettings = copySqlSettingsFrom(settings);
  const sqlSettings = Object.keys(copySettings)
    .map((key) => `${key} ${copySettings[key]}`)
    .join(', ');

  return trimQuery(`
    COPY ${tableNameInPostgres(tableId)} (${sqlColumns})
    FROM STDIN
    WITH (${sqlSettings});
  `);
};
