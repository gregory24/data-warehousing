import { ColumnDefaultValue, ColumnType, IColumn } from '../../../../data-shapes';
import {
  applicationToPostgresType,
  POSTGRES_FALSE_LITERAL,
  POSTGRES_NULL_LITERAL,
  POSTGRES_TRUE_LITERAL,
} from '../../data-types';
import { columnNameInPostgres } from '../../helpers';

export const columnDefaultValueInPostgresFor = (column: IColumn): string => {
  if (column.defaultValue === null) {
    return POSTGRES_NULL_LITERAL;
  }

  switch (column.type) {
    case ColumnType.Boolean:
      return column.defaultValue
        ? POSTGRES_TRUE_LITERAL
        : POSTGRES_FALSE_LITERAL;
    case ColumnType.Integer:
    case ColumnType.Float:
      return String(column.defaultValue); // NOT parsing, since the value could be a string (BigInt / BigFloat)
    case ColumnType.String:
      return `'${column.defaultValue}'`; // Quoting
    case ColumnType.Timestamp:
      if (column.defaultValue === ColumnDefaultValue.CurrentTimestamp) {
        return 'NOW()';
      }

      // SUPER-IMPORTANT: Using Floats to NOT use Millisecond-Precision
      return `TO_TIMESTAMP(${column.defaultValue}.0 / 1000.0)`;
    case ColumnType.SystemId:
      // Gen-Random-UUID is not supported by default
      //   - Using raw hashing function instead???
      return `CAST('${column.defaultValue}' AS UUID)`;
    case ColumnType.Json:
    default:
      // SUPER-IMPORTANT: JSON values MUST be quoted using '' -> Whatever goes INSIDE follows JSON-Format
      return `'${JSON.stringify(column.defaultValue)}'`; // Making JSON-Compatible depending on the data-type
  }
};

export const columnDefinitionInPostgres = (column: IColumn): string => {
  const columnParameters: string[] = [
    columnNameInPostgres(column.name),
    applicationToPostgresType(column.type),
  ];

  if (!column.isNullable) {
    columnParameters.push('NOT NULL');
  }

  if (column.defaultValue !== undefined) {
    columnParameters.push(`DEFAULT ${columnDefaultValueInPostgresFor(column)}`);
  }

  return columnParameters.join(' ');
};
