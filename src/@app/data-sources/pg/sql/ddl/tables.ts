import { isTemporaryTable, ITableId } from '../../../../data-tables';
import { tableNameInPostgres } from '../../helpers';
import { IDataShape } from '../../../../data-shapes';
import { trimQuery } from '../../../common';
import { columnDefinitionInPostgres } from './columns';

export const tableHeadingInPostgres = (tableId: ITableId): string =>
  isTemporaryTable(tableId)
    ? `CREATE TEMPORARY TABLE ${tableNameInPostgres(tableId)}`
    : `CREATE TABLE ${tableNameInPostgres(tableId)}`;

export const createTableFromSchemaInPostgresSql = (
  tableName: ITableId,
  shape: IDataShape,
): string => {
  const tableHeading = tableHeadingInPostgres(tableName);
  const columnDefinitions = shape.columns.map(columnDefinitionInPostgres);

  return trimQuery(
    `${tableHeading} (${columnDefinitions}) ${
      isTemporaryTable(tableName) ? ' ON COMMIT DROP' : ''
    }`,
  );
};

export const createTableInPostgresSql = (
  tableId: ITableId,
  selection: string,
): string => {
  const tableHeading = tableHeadingInPostgres(tableId);

  const tmpModifier = isTemporaryTable(tableId) ? ' ON COMMIT DROP' : '';

  // Special-Case: NOT Nesting With-Statements
  //    - Impossible to perform write-queries otherwise (MUST BE AT TOP-LEVEL)
  //    -> Assuming all the projection / selection / wrapping to be done inside
  const dataSelection =
    selection.search('WITH') !== -1
      ? selection
      : `WITH data AS (${selection}) SELECT * FROM data`;

  // Important: Using CTE to support write-queries (With Returning-Statement)
  return trimQuery(`${tableHeading}${tmpModifier} AS (${dataSelection})`);
};

export const dropTableInPostgresSqlFrom = (
  table: ITableId,
  doFailIfNotFound: boolean,
) => {
  const ifExistsStatement = doFailIfNotFound ? '' : 'IF EXISTS';
  return trimQuery(
    `DROP TABLE ${ifExistsStatement} ${tableNameInPostgres(table)}`,
  );
};
