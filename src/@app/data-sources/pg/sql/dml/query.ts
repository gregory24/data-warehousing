import { ITableId } from '../../../../data-tables';
import { IColumnName } from '../../../../data-shapes';
import { columnNameInPostgres, tableNameInPostgres } from '../../helpers';
import { trimQuery } from '../../../common';

export type IPgQueryOptions = Readonly<{
  columns?: IColumnName[];
  filterBy?: IColumnName[];
  limit?: number;
}>;

export const queryInPostgresSql = (
  tableId: ITableId,
  options?: IPgQueryOptions,
): string => {
  const selection = options?.columns
    ?.map(columnNameInPostgres)
    .join(', ');

  const conditions = options?.filterBy
    ?.map((c, i) => `${columnNameInPostgres(c)} = $${i + 1}`)
    .join(' AND ');

  const columnsList = selection ? selection : '*';
  const whereClause = conditions ? `WHERE ${conditions}` : '';
  const limitClause = options?.limit ? `LIMIT ${options?.limit}` : '';

  return trimQuery(`
    SELECT ${columnsList}
    FROM ${tableNameInPostgres(tableId)}
    ${whereClause}
    ${limitClause}
  `);
};
