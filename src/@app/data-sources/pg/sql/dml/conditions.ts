import { IColumnName } from '../../../../data-shapes';
import { ITableId } from '../../../../data-tables';
import { columnNameInPostgres, tableNameInPostgres } from '../../helpers';

export const matchKeysConditionInPostgresFrom = (
  sourceTable: ITableId,
  destinationTable: ITableId,
  keyColumns: IColumnName[],
): string => {
  const sourceTableName = tableNameInPostgres(sourceTable);
  const destinationTableName = tableNameInPostgres(destinationTable);

  const keyMatches = keyColumns.map(columnNameInPostgres).map(
    (column: IColumnName) =>
      `${sourceTableName}.${column} = ${destinationTableName}.${column}`,
  );

  return keyMatches.join(' AND ');
};

export const modifiedConditionInPostgresFrom = (
  sourceTable: ITableId,
  destinationTable: ITableId,
  valueColumns: IColumnName[],
  mergeNulls: boolean,
): string => {
  const sourceTableName = tableNameInPostgres(sourceTable);
  const destinationTableName = tableNameInPostgres(destinationTable);

  const columnChecks = valueColumns.map(columnNameInPostgres).map((column: IColumnName) => {
    const src = `${sourceTableName}.${column}`;
    const dest = `${destinationTableName}.${column}`;

    // Merge
    // NOT allowing replacing with NULLs (In ALL cases)
    //   1) Either Destination is empty -> NO need to chek further
    //   2) The value is different (Since neither Source nor Destination are NULLs, the checking is proper)
    if (mergeNulls) {
      return `(${src} IS NOT NULL AND (${dest} IS NULL OR ${src} != ${dest}))`;
    }

    // Full
    // NOT allowing to replacing with same values (Which could be NULLs)
    //   1) Replacing NULL with Value
    //   2) Replacing Value with NULL
    //   3) Replacing Value with Value
    //  Note: NULL != NULL -> False
    return `((${src} IS NULL AND ${dest} IS NOT NULL) OR (${src} IS NOT NULL AND ${dest} IS NULL) OR (${src} != ${dest}))`;
  });

  return columnChecks.join(' OR ');
};
