import { Readable } from 'stream';
import { PoolClient } from 'pg';
import QueryStream from 'pg-query-stream';
import { IPgQueryOptions, queryInPostgresSql } from '../sql';
import { ITableId } from '../../../data-tables';

export const DEFAULT_POSTGRES_CURSOR_READ_BATCH_SIZE = 500;

export const readStreamFromTableInPostgres = async (
  client: PoolClient,
  tableId: ITableId,
  query?: IPgQueryOptions,
  closingCallback?: (error?: Error) => Promise<void> | void,
): Promise<Readable> => {
  const sql = queryInPostgresSql(tableId, query);

  const stream = client.query(
    new QueryStream(sql, [], {
      batchSize: DEFAULT_POSTGRES_CURSOR_READ_BATCH_SIZE,
    }),
  );

  if (closingCallback) {
    stream.on('end', closingCallback);
    stream.on('error', closingCallback);
  }

  return stream;
};
