export function trimByRegexp(stringRegexp = '') {
  const frontPattern = `^${stringRegexp}`;
  const front = new RegExp(frontPattern, 'igm');

  const endPattern = `${stringRegexp}\$`;
  const end = new RegExp(endPattern, 'igm');

  return (value: string) => `${value}`.replace(front, '').replace(end, '');
}

export const trimQuery = trimByRegexp('[;\\s\\n\\r]+');
