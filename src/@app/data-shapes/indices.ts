import { IColumnName } from './columns';

export type IIndexId = string;

export const ascendingIndexOrdering = 'asc';
export const descendingIndexOrdering = 'desc';
export type IIndexOrdering = 'asc' | 'desc';

export interface IIndexKey {
  column: IColumnName;
  ordering: IIndexOrdering;
}

export interface IIndex {
  id: IIndexId;
  keys: IIndexKey[];
  isUnique?: boolean;
}
