import { IColumn, IColumnName } from './columns';
import { IIndex } from './indices';

export type IDataShape = Readonly<{
  columns: IColumn[];
  keys: IColumnName[];
  indices: IIndex[]; // TODO @@@@slava [high] re-think: do we need indices?
}>;
