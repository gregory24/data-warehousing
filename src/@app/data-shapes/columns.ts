import { ColumnType } from './column-type';

// Defaults enum

export enum ColumnDefaultValue {
  CurrentTimestamp = 'current-timestamp',
}

// Abstract-Type

export type IColumnName = string;

// Definition

export interface IBaseColumn {
  name: IColumnName;
  type: ColumnType;
  isNullable: boolean;
}

export interface IIntegerColumn extends IBaseColumn {
  type: ColumnType.Integer;
  defaultValue?: number;
}

export interface IFloatColumn extends IBaseColumn {
  type: ColumnType.Float;
  defaultValue?: number;
}

export interface IStringColumn extends IBaseColumn {
  type: ColumnType.String;
  defaultValue?: string;
}

export interface IBooleanColumn extends IBaseColumn {
  type: ColumnType.Boolean;
  defaultValue?: boolean;
}

export interface ITimestampColumn extends IBaseColumn {
  type: ColumnType.Timestamp;
  defaultValue?: number | ColumnDefaultValue.CurrentTimestamp; // UNIX Timestamp
}

export interface IJsonColumn extends IBaseColumn {
  type: ColumnType.Json;
  defaultValue?: any;
}

export interface ISystemIdColumn extends IBaseColumn {
  type: ColumnType.SystemId;
  defaultValue?: any;
}

export type IColumn =
  | IIntegerColumn
  | IFloatColumn
  | IStringColumn
  | IBooleanColumn
  | ITimestampColumn
  | IJsonColumn
  | ISystemIdColumn;

// Helpers

export const columnName = (column: IColumn): IColumnName => column.name;

export const columnNames = (columns: IColumn[]): IColumnName[] =>
  columns.map(columnName);
