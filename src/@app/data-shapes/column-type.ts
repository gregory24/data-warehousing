export enum ColumnType {
  Integer = 'integer',
  Float = 'float',
  String = 'string',
  Boolean = 'boolean',
  Timestamp = 'timestamp',
  Json = 'json',
  SystemId = 'system-id', // UUID (Generated and provided by the system)
}
