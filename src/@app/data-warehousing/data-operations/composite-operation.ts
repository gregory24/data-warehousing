import {
  IDataOperation,
  IDataOperationWithNoneOutput,
  IDataOperationWithTableOutput,
  IDataOperationWithValueOutput,
} from './operations';
import { IDataSource } from '../../data-sources';
import { IDataOperationProvider, IDataOperationProviders } from './models';

export function CompositeDataOperation(
  operations: IDataOperation<any, any>[],
): IDataOperationWithNoneOutput<void>;
export function CompositeDataOperation<Input, Output>(
  operations: IDataOperation<any, any>[],
  resultOperation: IDataOperationWithValueOutput<Input, Output>,
): IDataOperationWithValueOutput<Input, Output>;
export function CompositeDataOperation<Input>(
  operations: IDataOperation<any, any>[],
  resultOperation: IDataOperationWithTableOutput<Input>,
): IDataOperationWithTableOutput<Input>;
export function CompositeDataOperation<
  Input,
  Output = any,
>(
  operations: IDataOperation<any, any>[],
  resultOperation?:
    | IDataOperationWithValueOutput<Input, Output>
    | IDataOperationWithTableOutput<Input>,
): IDataOperation<any, any> {
  const allOperations = [...operations, resultOperation].filter(Boolean);

  if (allOperations.length === 0) {
    throw new Error('No operations provided for composition');
  }

  const providerForDataSource = (
    dataSource: IDataSource,
  ): IDataOperationProvider<any> => () =>
    // IMPORTANT: Using Flat-Map to account for nested Composite-Operations
    allOperations.flatMap((operation: IDataOperation<any, any>) =>
      operation.providers[dataSource](operation.input)
    );

  const providers = Object.keys(allOperations[0].providers).reduce(
    (providers: IDataOperationProviders<any>, dataSource: IDataSource) => ({
      ...providers,
      [dataSource]: providerForDataSource(dataSource),
    }),
    {} as IDataOperationProviders<any>,
  );

  if (!resultOperation) {
    return { type: 'none', providers };
  }

  if (resultOperation.type === 'table') {
    return { type: 'table', providers, table: resultOperation.table };
  }

  return { type: 'value', providers, mapper: resultOperation.mapper };
}
