// Definitions

// Only an SQL query
export type ISingleDataOperationConfigWithQuery = string;

// SQL query with parameters
export type ISingleDataOperationConfigWithQueryAndParameters = Readonly<{
  query: string;
  values?: any[];
}>;

// Configuration of a single operation
export type ISingleDataOperationConfig =
  | ISingleDataOperationConfigWithQuery
  | ISingleDataOperationConfigWithQueryAndParameters;

// Configuration of multiple operations
export type ICompositeDataOperationConfig = IDataOperationConfig[];

// Configuration of operation
export type IDataOperationConfig =
  | ISingleDataOperationConfig
  | ICompositeDataOperationConfig;

// Helpers

export const isDataOperationConfigWithParameters = (
  config: ISingleDataOperationConfig,
): config is ISingleDataOperationConfigWithQueryAndParameters =>
  !(typeof config === 'string');

export const isCompositeDataOperationConfig = (
  config: IDataOperationConfig,
): config is ICompositeDataOperationConfig =>
  Array.isArray(config);
