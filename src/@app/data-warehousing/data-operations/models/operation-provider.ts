import { IDataOperationConfig } from './operation-config';
import { IDataSource } from '../../../data-sources';

export type IDataOperationProvider<Input> = (
  input: Input,
  withOutput?: boolean, // Specifies whether output needs to be returned
) => IDataOperationConfig | IDataOperationConfig[];

export type IDataOperationProviders<Input> =
  Readonly<Record<IDataSource, IDataOperationProvider<Input>>>;
