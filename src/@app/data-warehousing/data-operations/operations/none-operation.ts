import { IDataOperationProviders } from '../models';

// Operation with none output

export type IDataOperationWithNoneOutput<Input> =
  Readonly<{
    type: 'none';
    input?: Input;
    providers: IDataOperationProviders<Input>;
  }>;
