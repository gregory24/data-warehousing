import { ITableRecord } from '../../../data-tables';
import { IDataOperationProviders } from '../models';

// Operation with output of mapped values

// Mapper for translating output of a query into results
export type IDataOperationMapper<
  Input,
  Output = void,
> = (
  results: ITableRecord[],
  input?: Input,
) => Output;


export type IDataOperationWithValueOutput<
  Input,
  Output,
> = Readonly<{
  type: 'value';

  input?: Input;
  providers: IDataOperationProviders<Input>;

  mapper?: IDataOperationMapper<Input, Output>;
}>;
