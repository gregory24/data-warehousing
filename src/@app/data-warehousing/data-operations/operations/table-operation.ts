import { ITableId } from '../../../data-tables';
import { IDataOperationProviders } from '../models';

// Operation with table output

export type IDataOperationWithTableOutput<Input> =
  Readonly<{
    type: 'table';

    input?: Input;
    providers: IDataOperationProviders<Input>;

    table: ITableId;
  }>;
