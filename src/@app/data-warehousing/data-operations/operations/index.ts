import { IDataOperationWithNoneOutput } from './none-operation';
import { IDataOperationWithValueOutput } from './value-operation';
import { IDataOperationWithTableOutput } from './table-operation';

export type IDataOperation<Input, Output> =
  | IDataOperationWithNoneOutput<Input>
  | IDataOperationWithValueOutput<Input, Output>
  | IDataOperationWithTableOutput<Input>;

export * from './none-operation';
export * from './value-operation';
export * from './table-operation';
