import { PersistentTableId, TemporaryTableId, ITableRecord } from '../../data-tables';
import { IDataOperationProvider, IDataOperationProviders } from './models';
import {
  IDataOperationMapper,
  IDataOperationWithNoneOutput,
  IDataOperationWithTableOutput,
  IDataOperationWithValueOutput,
} from './operations';

// Abstraction over provision of operations

export interface IDataOperationFactory<Input, Output> {
  // NOT outputting anything from a query
  none(): IDataOperationWithNoneOutput<Input>;

  // Outputting results from a query directly as values -> Mapping
  value(): IDataOperationWithValueOutput<Input, Output>;

  // Output results to a table
  // Note: No namespace -> Temporary
  table(namespace?: string, id?: string): IDataOperationWithTableOutput<Input>;
}


// Wrapper for providing different types of operations with given providers

export function DataOperation<
  Input,
  Output = ITableRecord[], // Outputting raw records by default
>(
  providers: IDataOperationProviders<Input>,
  mapper?: IDataOperationMapper<Input, Output>,
) {
  return (
    input: Input,
  ): IDataOperationFactory<Input, Output> => {
    const none = (): IDataOperationWithNoneOutput<Input> => {
      return { type: 'none', input, providers };
    };

    const value = (): IDataOperationWithValueOutput<
      Input,
      Output
      > => {
      return { type: 'value', input, providers, mapper };
    };

    const table = (
      namespace?: string,
      id?: string,
    ): IDataOperationWithTableOutput<Input> => {
      const table = namespace
        ? PersistentTableId(namespace, id)
        : TemporaryTableId();

      return { type: 'table', input, providers, table };
    };

    return { none, value, table };
  };
}
