import { ITableName } from '@app/data-tables/drivers/pg/sql';
import { Client, PoolClient } from 'pg';
import {
  IDataOperation,
  IDataOperationWithNoneOutput,
  IDataOperationWithTableOutput,
  IDataOperationWithValueOutput,
} from './models';
import { dataOperationQueryFrom, dataOperationValuesFrom } from './helpers';

// SUPER-IMPORTANT: Large Refactoring for Abstraction Data-Warehouses
//   -> Instances, Connections, Transactions, etc.

// -----------------------------------------------------------------------------
// Executioner Abstraction
// -----------------------------------------------------------------------------

export interface IDataOperationExecutioner {
  execute<Input>(
    operation: IDataOperationWithNoneOutput<Input>,
  ): Promise<void>;
  execute<Input, Output>(
    operation: IDataOperationWithValueOutput<Input, Output>,
  ): Promise<Output>;
  execute<Input>(
    operation: IDataOperationWithTableOutput<Input>,
  ): Promise<ITableName>;
}

// -----------------------------------------------------------------------------
// Operation Executioners
// -----------------------------------------------------------------------------

// Executing a query with none output

export const executeWithNoneOutput = async <Input>(
  client: Client | PoolClient,
  operation: IDataOperationWithNoneOutput<Input>,
): Promise<void> => {
  await client.query({
    text: dataOperationQueryFrom(operation),
    values: dataOperationValuesFrom(operation),
  });
};

// Executing a query with value output

export const executeWithValueOutput = async <
  Input,
  Output,
>(
  client: Client | PoolClient,
  operation: IDataOperationWithValueOutput<Input, Output>,
): Promise<Output> => {
  const { rows: results } = await client.query({
    text: dataOperationQueryFrom(operation),
    values: dataOperationValuesFrom(operation),
  });

  return (
    operation.mapper ? operation.mapper(results, operation.input) : results
  ) as any;
};

// Executing a query with table output

export const executeWithTableOutput = async <Input>(
  client: Client | PoolClient,
  operation: IDataOperationWithTableOutput<Input>,
): Promise<ITableName> => {
  await client.query({
    text: dataOperationQueryFrom(operation),
    values: dataOperationValuesFrom(operation),
  });

  return operation.table;
};

// -----------------------------------------------------------------------------
// Executioner in Postgres
// -----------------------------------------------------------------------------

export const PostgresDataOperationExecutioner = (
  client: Client | PoolClient,
): IDataOperationExecutioner => {
  const execute = (operation: IDataOperation<any>): any => {
    if (operation.type === 'none') {
      return executeWithNoneOutput(client, operation);
    }

    if (operation.type === 'value') {
      return executeWithValueOutput(client, operation);
    }

    return executeWithTableOutput(client, operation);
  };

  return { execute };
};
