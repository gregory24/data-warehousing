// REFACTORING: Merging with Data-Tables

// Column Model

import { IColumnName } from '@app/data-shapes';
import { IMappedColumn } from '@app/data-tables';
import { columnNameInPostgres } from '@app/@packages/pg';

export type IOperationalColumn = IColumnName | IMappedColumn;

// Column Helpers

export const mappedColumnFrom = (column: IOperationalColumn): IMappedColumn =>
  typeof column === 'string' ? { source: column, destination: column } : column;

export const mappedColumnsFrom = (
  columns: IOperationalColumn[],
): IMappedColumn[] => columns.map(mappedColumnFrom);

export const mappedColumnInPostgres = (
  column: IMappedColumn,
): IMappedColumn => ({
  source: columnNameInPostgres(column.source),
  destination: columnNameInPostgres(column.destination),
});

export const operationalColumnInPostgres = (
  column: IOperationalColumn,
): IMappedColumn => mappedColumnInPostgres(mappedColumnFrom(column));
