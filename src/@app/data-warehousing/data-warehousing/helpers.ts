import { IDataOperation } from './models';
import { trimQuery } from '@app/@packages/pg';
import { createTableInPostgresSqlFrom } from '@app/data-tables/drivers/pg/sql';

export const dataOperationQueryFrom = (
  operation: IDataOperation<any>,
): string => {
  // Preparing the query itself
  const query = trimQuery(
    typeof operation.config === 'string'
      ? operation.config
      : operation.config.query,
  );

  // Removing the semicolon if needed
  const formattedQuery =
    query[query.length - 1] === ';' ? query.slice(0, query.length - 1) : query;

  // Table-Operations: Wrapping into a Table-Creation statement
  if (operation.type === 'table') {
    return createTableInPostgresSqlFrom(operation.table, formattedQuery);
  }

  // None-Operation / Value-Operation: Simply returning the query
  return formattedQuery + ';';
};

export const dataOperationValuesFrom = ({
  config,
}: IDataOperation<any>): any[] | undefined => {
  if (typeof config === 'string') {
    return undefined;
  }

  return config.values ? config.values : undefined;
};
