import { Pool } from 'pg';
import { IPgDataWarehouseSettings, postgresPoolConfigFrom } from './pg.data-warehouse-settings';
import { IDataWarehouse, IDataWarehouseConnectionOptions } from '../data-warehouse';
import { IDataWarehouseExecutor } from '../data-warehouse-executor';

export const PgDataWarehouse = (
  settings: IPgDataWarehouseSettings,
): IDataWarehouse => {
  const pool = new Pool(postgresPoolConfigFrom(settings));

  const get = async ({
    tx,
  }: IDataWarehouseConnectionOptions): Promise<IDataWarehouseExecutor> => {
    const client = await pool.connect();

    throw 1;
  };

  const close = async (): Promise<void> => {
    await pool.end();
  };

  return { get, close };
};
