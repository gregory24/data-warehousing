// Settings

import { PoolConfig } from 'pg';

export type IPgDataWarehousePoolingSettings = Readonly<{
  max?: number;
  idleTimeout?: number;
}>;

export type IPgDataWarehouseSettings = Readonly<{
  host: string;
  port: number;
  user: string;
  password: string;
  database: string;

  pooling?: IPgDataWarehousePoolingSettings;
}>;

// Defaults

export const DEFAULT_PG_POOLING_SETTINGS: IPgDataWarehousePoolingSettings = {
  max: 10, // Keeping 10 connections open at most
  idleTimeout: 10 * 1000, // Closing idle connections in 10s
};

// Configuration

export const postgresPoolConfigFrom = ({
  pooling,
  ...credentials
}: IPgDataWarehouseSettings): PoolConfig => {
  const { max, idleTimeout } = {
    ...DEFAULT_PG_POOLING_SETTINGS,
    ...(pooling || {}),
  };

  return {
    ...credentials,
    max,
    idleTimeoutMillis: idleTimeout,
  };
};

