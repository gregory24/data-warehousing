import { IDataWarehouseExecutor } from './data-warehouse-executor';

export type IDataWarehouseConnectionOptions = Readonly<{
  tx?: boolean; // Specifies whether transaction behavior should be used
}>;

export interface IDataWarehouse {
  get(
    options: IDataWarehouseConnectionOptions,
  ): Promise<IDataWarehouseExecutor>;

  close(): Promise<void>;
}
