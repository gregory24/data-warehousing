import { Readable } from 'stream';
import { ITableId, ITableRecord } from '../../data-tables';
import {
  IDataOperationWithNoneOutput,
  IDataOperationWithTableOutput,
  IDataOperationWithValueOutput,
} from '../data-operations/operations';

export interface IDataWarehouseExecutor {
  // Reading records from the table as a stream
  // TODO: "query" + Extra options (Very generic)
  read(table: ITableId): Promise<Readable>;
  readBuffer(table: ITableId): Promise<ITableRecord>;

  // Offloading data into a table (Creating if no checking is needed)
  // TODO: Data-Shape
  write(table: ITableId, data: Readable, check?: boolean): Promise<void>;
  writeBuffer(
    table: ITableId,
    data: ITableRecord[],
    check?: boolean,
  ): Promise<void>;

  // Running an operation
  execute<Input>(operation: IDataOperationWithNoneOutput<Input>): Promise<void>;
  execute<Input>(
    operation: IDataOperationWithTableOutput<Input>,
  ): Promise<ITableId>;
  execute<Input, Output>(
    operation: IDataOperationWithValueOutput<Input, Output>,
  ): Promise<Output>;

  // Finalizing the connection / transaction
  finish(error?: Error): Promise<void>;
}
